/**
 * @author Anastasiia Harmash
 * @version 15 nov 2019
 */
package com.anastasiia.passive.controller;

import com.anastasiia.passive.model.Bouguet;

/*
 * Controller calls the methods from model
 */

public class Controller {

    public static void order() {
        Bouguet.makeOrder();
    }

    public static void showAssortment() {
        Bouguet.showAssortment();
    }

}
