/**
 * @author Anastasiia Harmash
 * @version 15 nov 2019
 */
package com.anastasiia.passive.model;

import com.anastasiia.passive.view.View;

/**
 * Calls the method View
 */

public class Application {

    public static void main(String[] args) {

        View.menu();

    }
}
