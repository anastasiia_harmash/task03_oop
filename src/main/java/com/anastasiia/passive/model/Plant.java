/**
 * @author Anastasiia Harmash
 * @version 15 nov 2019
 */

package com.anastasiia.passive.model;

public class Plant {
    private String name;
    private int price;

    public Plant(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return name + ", price=" + price + " ";
    }
}
