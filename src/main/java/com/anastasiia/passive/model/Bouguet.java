/**
 * @author Anastasiia Harmash
 * @version 15 nov 2019
 */
package com.anastasiia.passive.model;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Class Bouquet has Linked Map of objects,
 * method showAssortment, method makeOrder
 */

public class Bouguet {

    static Flower Rose = new Flower("Rose", 20, "red");
    static Flower Tulip = new Flower("Tulip", 15, "yellow");
    static Flower Peony = new Flower("Peony", 25, "pink");

    static FlowerPot Lavender = new FlowerPot("Lavender", 60, 10);
    static FlowerPot Succulent = new FlowerPot("Succulent", 50, 3);
    static FlowerPot Heather = new FlowerPot("Heather", 40, 7);

    public static Map<Integer, Object> assortment;

    static {
        assortment = new LinkedHashMap<Integer, Object>();
        assortment.put(1, Rose);
        assortment.put(2, Tulip);
        assortment.put(3, Peony);
        assortment.put(4, Lavender);
        assortment.put(5, Succulent);
        assortment.put(6, Heather);
    }

    public static void showAssortment() {
        System.out.println(Bouguet.assortment.get(1));
        System.out.println(Bouguet.assortment.get(2));
        System.out.println(Bouguet.assortment.get(3));
        System.out.println(Bouguet.assortment.get(4));
        System.out.println(Bouguet.assortment.get(5));
        System.out.println(Bouguet.assortment.get(6));
        System.out.println();
    }

    public static void makeOrder() {
        System.out.println("Please, write how many flowers do you want to buy");
        System.out.println("Rose: ");
        Scanner k = new Scanner(System.in);
        int roseNum = k.nextInt();
        int roseSum = roseNum * Rose.getPrice();

        System.out.println("Tulip: ");
        int tulipNum = k.nextInt();
        int tulipSum = tulipNum * Tulip.getPrice();

        System.out.println("Peony: ");
        int peonyNum = k.nextInt();
        int peonySum = peonyNum * Peony.getPrice();

        System.out.println("Lavender: ");
        int lavenderNum = k.nextInt();
        int lavenderSum = lavenderNum * Lavender.getPrice();

        System.out.println("Succulent: ");
        int succulentNum = k.nextInt();
        int succulentSum = succulentNum * Succulent.getPrice();

        System.out.println("Heather: ");
        int heatherNum = k.nextInt();
        int heatherSum = heatherNum * Heather.getPrice();

        int sumOfOrder = roseSum + tulipSum + peonySum + lavenderSum + succulentSum + heatherSum;

        System.out.println("Amount of your order = " + sumOfOrder);

    }
}
