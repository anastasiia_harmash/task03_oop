/**
 * @author Anastasiia Harmash
 * @version 15 nov 2019
 */
package com.anastasiia.passive.model;

public class Flower extends Plant {
    private String color;

    public Flower(String name, int price, String color) {
        super(name, price);
        this.color = color;
    }

    @Override
    public String toString() {
        return super.toString() + "color='" + color + '\'';
    }
}
