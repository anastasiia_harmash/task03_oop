/**
 * @author Anastasiia Harmash
 * @version 15 nov 2019
 */
package com.anastasiia.passive.view;

import com.anastasiia.passive.controller.Controller;

import java.util.Scanner;

/**
 * class View show menu to users,
 * calls  methods from Controller
 */

public class View {

    public static void menu() {

        int menuPoint;
        do {

            System.out.println("1 - Show assortment");
            System.out.println("2 - Make order");
            System.out.println("3 - exit");
            System.out.println("Please, make your choice:");

            Scanner k = new Scanner(System.in);
            menuPoint = k.nextInt();

            if (menuPoint == 1) {
                Controller.showAssortment();
            } else {
                if (menuPoint == 2) {
                    Controller.order();
                }
            }
        } while (menuPoint != 3);

    }


}
