/**
 * @author Anastasiia Harmash
 * @version 15 nov 2019
 */
package com.anastasiia.passive.model;

public class FlowerPot extends Plant {

    private int size;

    public FlowerPot(String name, int price, int size) {
        super(name, price);
        this.size = size;
    }

    @Override
    public String toString() {
        return super.toString() + "size=" + size;
    }
}
